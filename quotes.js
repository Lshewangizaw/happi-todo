'use strict';

let quotes = [
  {
    author: 'Audrey Hepburn' ,
    text: 'Nothing is impossible, the word itself says \'I\'m possible\'!'
  } ,
  {
    author: 'Walt Disney' ,
    text: 'You may not realize it when it happens, but a kick in the teeth may be the best thing in the world for you'
  } ,
  {
    author: 'Unknown' ,
    text: 'Even the greatest was once a beginner. Don\'t be afraid to take that first step'
  } ,
  {
    author: 'Neale Donald Walsh' ,
    text: 'You are afraid to die, and you\'re afraid to live. What a way to exist'
  }
];

const Hapi = require('hapi');
const Joi = require('joi');
const server = new Hapi.Server();
server.connection ({ port:1337, host: '127.0.0.1' });
server.route ({
  method: 'GET',
  path: '/quotes',
  handler: function(request, reply) {
    reply(quotes);
  }
});

server.route ({
  method: 'GET',
  path: '/random',
  handler: function (request,reply) {
    let id = Math.floor(Math.random() * quotes.length);
    reply(quotes[id]);
  }
});

server.route ({
  method: 'GET',
  path: '/quote/{id?}',
  handler: function (request, reply) {
    if (request.params.id) {
      if (quotes.length <= request.params.id) {
        return reply('No quote found.').code(404);
      }
      return reply(quotes[request.params.id]);
    }
    reply(quotes);
  }
});

server.route({
  method: 'POST',
  path: '/quote',
  config: {
    handler: function(request, reply) {
      let newQuote = {
        author: request.payload.author,
        text: request.payload.text
      };
      quotes.push(newQuote);
      reply(newQuote);
    },
    validate: {
      payload: {
        author: Joi.string().required(),
        text: Joi.string().required()
      }
    }
  }
});

server.route ({
  method: 'DELETE',
  path: '/quote/{id}',
  handler: function(request, reply) {
    if(quotes.length <= request.params.id) {
      return reply ('No quote found.').code(404);
    }
    quotes.splice(request.params.id, 1);
    reply(true);
  }
});

server.start((err) => {
  if(err) {
    throw err;
  }
  console.log(`Server running at ${server.info.uri}`);
});
