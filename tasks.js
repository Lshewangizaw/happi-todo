'use strict';

let tasks = ["test1", "test2", "test3"];

const Hapi = require('hapi');
const Joi = require('joi');
const server = new Hapi.Server();
server.connection ({ port:1337, host: '127.0.0.1' });

//As a user I can view all tasks I've created
server.route ({
  method: 'GET',
  path: '/',
  handler: function(request, reply) {
    reply(tasks);
  }
})

server.route ({
  method: 'POST',
  path: '/add',
  config: {
    handler: function(request, reply){
      let newTask = request.payload;
      tasks.push(newTask);
      reply(newTask);
    },
  validate: {
    payload: Joi.string().required()
    }
}
});

server.route ({
  method: 'DELETE',
  path: '/delete/{id}',
  handler: function(request,reply) {
    if(tasks.length <= request.params.id) {
      return reply('No such task exists.').code(404);
    }
    tasks.splice(request.params.id, 1);
    reply(true);
  }
});

server.start((err) => {
  if(err) {
    throw err;
  }
  console.log(`Server running at ${server.info.uri}`);
});
